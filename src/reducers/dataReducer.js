import {mergeRight} from "ramda";
import {actionTypes} from "../constants";

const initState = {averageAmount: null, selectedRating: null};

const dataReducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.DATA_GET_AVERAGE_AMOUNT_ACTION:
      return mergeRight(state, {averageAmount: action.payload});
    case actionTypes.DATA_SELECTED_RATING_ACTION:
      return mergeRight(state, {selectedRating: action.payload});
    default:
      return state;
  }
}

export default dataReducer;