import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import dataReducer from "./dataReducer";
import apiReducer from "./apiReducer";


export const rootReducer = combineReducers({
  routing: routerReducer,
  data: dataReducer,
  api: apiReducer
});