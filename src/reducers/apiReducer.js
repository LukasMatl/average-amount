import {mergeRight} from "ramda";
import {actionTypes} from "../constants";

const apiReducer = (state = {loading: false, error: false}, action) => {
  switch (action.type) {
    case actionTypes.API_LOADING:
      return mergeRight(state,{loading: true, error: false});
    case actionTypes.API_STOP_LOADING:
      return mergeRight(state,{loading: false});
    case actionTypes.API_STOP_LOADING_WITH_ERROR:
      return mergeRight(state,{loading: false, error: true});
    default:
      return state;
  }
}
export default apiReducer;