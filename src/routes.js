import React from 'react';
import {Route} from 'react-router';
import AverageAmount from "./containers/AverageAmount";

const routes = [
  <Route key='1' exact path={'/'} component={AverageAmount}/>,
];

export default routes;
