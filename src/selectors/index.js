import {createSelector} from 'reselect';
import {
  identity, prop, path
} from 'ramda';

// api
export const getApi = createSelector(path(['api']), identity);
export const getLoadingState = createSelector([getApi], prop('loading'));

// data
export const getData = createSelector(path(['data']), identity);
export const getAverageAmount = createSelector([getData], prop('averageAmount'));
export const getSelectedRating = createSelector([getData], prop('selectedRating'));