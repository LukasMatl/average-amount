import React from 'react';
import {render} from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import Root from './containers/Root';
import configureStore from './store';
import routes from "./routes";

const rootElement = document.getElementById('root');
let initialStoreData = {};

const store = configureStore(initialStoreData);

render(
    <AppContainer>
      <Root store={store}  routes={routes}/>
    </AppContainer>,
    rootElement,
);


