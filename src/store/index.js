import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {rootReducer} from "../reducers";

const configureStore = (preloadedState = {}) => {
  return createStore(
      rootReducer,
      preloadedState,
      applyMiddleware(logger, thunk)
  )
}

export default configureStore;