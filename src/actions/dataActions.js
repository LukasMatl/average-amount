import axios from 'axios';
import {actionTypes, API_URL, SIZE_HEADER, TOTAL_HEADER} from "../constants";
import {loading, stopLoading, stopLoadingWithError} from "./apiActions";
import {divide, pipe, sum, map} from "ramda";
import createAction from "redux-actions/es/createAction";

const getJsonLoans = (ratingCode, action, headers = {}) => (dispatch) => axios.get(API_URL, {
  params: {
    rating__eq: ratingCode
  },
  headers
}).then(({data,  headers}) => {
  dispatch(action(data, headers, ratingCode));
});


const getAverageAmount = (data, headers, ratingCode) => (dispatch) => {
  const totalLoans = headers[TOTAL_HEADER];
  dispatch(getJsonLoans(ratingCode, setAverageAmount, {[SIZE_HEADER]: totalLoans}))
}


const setAverageAmount = (data, headers, ratingCode) => (dispatch) => {
  const totalLoans = headers[TOTAL_HEADER];
  const averageAmount = pipe(map((rec) => rec.amount), sum, (sum) => divide(sum,totalLoans))(data);
  dispatch(createAction(actionTypes.DATA_GET_AVERAGE_AMOUNT_ACTION)(averageAmount));
}

export const getAverageAmountPerRating = (ratingCode) => (dispatch) => {
    Promise.all([
        dispatch(loading()),
        dispatch(getJsonLoans(ratingCode, getAverageAmount))
    ]).then(() => {
      dispatch(stopLoading());
      dispatch(createAction(actionTypes.DATA_SELECTED_RATING_ACTION)(ratingCode))
    }).catch(() => dispatch(stopLoadingWithError()))
}




