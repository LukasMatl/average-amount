import createAction from "redux-actions/es/createAction";
import {actionTypes} from "../constants";

export const loading = () => (dispatch) => {
  dispatch(createAction(actionTypes.API_LOADING)());
}

export const stopLoading = () => (dispatch) => {
  dispatch(createAction(actionTypes.API_STOP_LOADING)());
}

export const stopLoadingWithError = () => (dispatch) => {
  dispatch(createAction(actionTypes.API_STOP_LOADING_WITH_ERROR)());
}