export const RATINGS = [
  {
    className: 'btn-aaaaa',
    code: 'AAAAA',
    name: '3,99%'
  },
  {
    className: 'btn-aaaa',
    code: 'AAAA',
    name: '4,99%'
  },
  {
    className: 'btn-aaa',
    code: 'AAA',
    name: '5,99%'
  },
  {
    className: 'btn-aa',
    code: 'AA',
    name: '8,49%'
  },
  {
    className: 'btn-a',
    code: 'A',
    name: '10,99%'
  },
  {
    className: 'btn-b',
    code: 'B',
    name: '13,49%'
  },
  {
    className: 'btn-c',
    code: 'C',
    name: '15,49%'
  },
  {
    className: 'btn-d',
    code: 'D',
    name: '19,99%'
  }
];

export const CZK_CURRENCY = 'Kč';

export  const API_URL = 'https://thingproxy.freeboard.io/fetch/https://app.zonky.cz/api/loans/marketplace?';

export const SIZE_HEADER = 'x-size';
export const TOTAL_HEADER = 'x-total';
export const AMOUNT_FIELD = 'amount';

export const actionTypes = {
  API_LOADING: 'API/LOADING',
  API_STOP_LOADING: 'API/STOP_LOADING',
  API_STOP_LOADING_WITH_ERROR: 'API/STOP_LOADING_WITH_ERROR',
  DATA_GET_AVERAGE_AMOUNT_ACTION: 'DATA/GET_AVERAGE_AMOUNT',
  DATA_SELECTED_RATING_ACTION: 'DATA/SELECTED_RATING',
}