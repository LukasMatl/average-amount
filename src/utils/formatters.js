import {CZK_CURRENCY} from "../constants";
import {isNil} from "ramda";

const asPrice = (value, currency) => {
  const parsedNumber = Number(value).toFixed(2);
  return `${parsedNumber} ${currency}`;
};

export const asCZKPrice = (value) => asPrice(value, CZK_CURRENCY);

