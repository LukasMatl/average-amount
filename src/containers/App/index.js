import React, { Component } from 'react';
import Header from "../../components/Header";


class App extends Component {

  componentDidMount = () => {
    require('../../scss/app.scss');
  }

  render() {
    const {children} = this.props;
    return (
          <div className="app">
            <Header/>
            <section className="content">
              {children}
            </section>
          </div>
    );
  }
}

export default App;
