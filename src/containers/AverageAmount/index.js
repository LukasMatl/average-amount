import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {applySpec, filter} from "ramda";
import ButtonGroup from "../ButtonGroup";
import {getAverageAmount, getLoadingState} from "../../selectors";
import {asCZKPrice} from "../../utils/formatters";


class AverageAmount extends Component {

  render() {
    const {averageAmount} = this.props;
    return (
        <div className="average-amount">
          <ButtonGroup/>
          <div className="price">
            <span>Průměrná výše půjček: </span>
            <span>{asCZKPrice(averageAmount)}</span>
          </div>
        </div>
    );
  }
}


const mapStateToProps = applySpec({
  averageAmount: getAverageAmount
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(AverageAmount);
