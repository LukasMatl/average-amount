import React, { Component } from 'react';
import {applySpec, map, equals} from "ramda";
import {RATINGS} from "../../constants";
import {getLoadingState, getSelectedRating} from "../../selectors";
import {bindActionCreators} from "redux";
import connect from "react-redux/es/connect/connect";
import {getAverageAmountPerRating} from "../../actions/dataActions";
import classNames from 'classnames';


class ButtonGroup extends Component {

  btnClassName = (selectedRating, config, disableButtons) => classNames('btn', {
    [config.className]: !equals(selectedRating, config.code) || disableButtons,
    [`${config.className}--active`]: equals(selectedRating, config.code) && !disableButtons
  });

  createBtn = (config) => {
    const {disableButtons, selectedRating, getAverageAmountPerRating} = this.props;
    return (
        <button
            className={this.btnClassName(selectedRating,config, disableButtons)}
            key={config.code}
            disabled={disableButtons}
            onClick={() => getAverageAmountPerRating(config.code)}
        >
          {config.name}
        </button>
    )
  }

  render() {
    return (
        <div className="button-group">
          <span>Roční úrok</span>
          {map((config) => this.createBtn(config), RATINGS)}
        </div>
    );
  }
}
const mapStateToProps = applySpec({
  disableButtons: getLoadingState,
  selectedRating: getSelectedRating
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({
    getAverageAmountPerRating
  }, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(ButtonGroup);
