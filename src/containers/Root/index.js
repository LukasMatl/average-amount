import React, {Component} from "react";
import {Provider} from "react-redux";
import App from "../App";
import {HashRouter, Switch} from "react-router-dom";

class Root extends Component {

  render() {
    const {store,  routes} = this.props;
    return (
        <Provider store={store}>
          <App>
            <HashRouter>
              <Switch>
                {routes}
              </Switch>

              </HashRouter>
          </App>
        </Provider>
    );
  }
}


export default Root;